Binary files stored as QR codes
===============================

Prepare environment
-------------------

1. Download [Python 3.x](https://www.python.org/downloads/)
    - `C:/your/python.exe` is used as example
    - Replace this path with yours like `C:/python37/python.exe` or `/usr/bin/python3`
2. Obtain [sources](https://bitbucket.org/kolinger/binary-qr/src/master/)
3. Only for encoding
    - Install dependencies `C:/your/python.exe -m pip install -r requirements.txt`

Encode
------

1. Run `C:/your/python.exe encode.py binary_lick.jpg`
2. QR codes as PNG files will be created in current working directory with name: `file_name.extension-index.png`
3. Enjoy!

Decode
------

1. Put all your scanned QR codes as binary blobs into `./source` directory
    - Note: QR codes needs to be scanned as binary blobs and not all QR readers support this,
      many of them will produce corrupted result - for example:
      `com.teacapps.barcodescanner` / `com.teacapps.barcodescanner.pro` works well
    - Note: order doesn't matter and multiple files can be decoded at the same time
2. Run `C:/your/python.exe decode.py ./source/* --target-dir ./result`
3. Decoded files will be created in `--target-dir` directory
4. Enjoy!
