import argparse
import base64
from glob import glob
import gzip
import os
import sys

parser = argparse.ArgumentParser()
parser.add_argument("source_glob")
parser.add_argument("--target-dir")
args = parser.parse_args()

buffer = {}
for path in glob(args.source_glob):
    with open(path, "rb") as file:
        payload = file.read()

    payload = gzip.decompress(payload)
    header = payload[:254]
    chunk = payload[254:]

    header = header.strip(b"\x00")
    chunk_name = base64.b64decode(header).decode("utf-8")
    file_name, chunk_index = chunk_name.rsplit(".", maxsplit=1)

    if file_name not in buffer:
        buffer[file_name] = []

    buffer[file_name].append((chunk_index, chunk))

target_dir = args.target_dir
if target_dir is None or target_dir == "":
    target_dir = "."

if not os.path.exists(target_dir):
    os.mkdir(target_dir)

for file_name, chunks in buffer.items():
    target_path = os.path.join(target_dir, file_name)

    if os.path.exists(target_path):
        print("ERROR: target file %s already exists" % target_path, file=sys.stderr)
        exit(1)

    chunks = sorted(chunks, key=lambda item: item[0])
    contents = bytearray()
    for chunk_index, chunk in chunks:
        contents.extend(chunk)

    with open(target_path, "wb") as file:
        file.write(contents)
