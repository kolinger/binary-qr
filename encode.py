import argparse
import base64
import gzip
import os
import sys

import segno

parser = argparse.ArgumentParser()
parser.add_argument("source_file")
args = parser.parse_args()

if not os.path.exists(args.source_file):
    print("ERROR: source file '%s' not found" % args.source_file, file=sys.stderr)
    exit(1)

directory = os.path.dirname(args.source_file)
if directory == "":
    directory = "."

file_name = os.path.basename(args.source_file)

chunks = []
with open(args.source_file, "rb") as file:
    while True:
        chunk = file.read(1024)
        if len(chunk) == 0:
            break
        chunks.append(chunk)

target_path = os.path.join(directory, "qr-" + file_name + "-%s.png")
for chunk_index, chunk in enumerate(chunks):
    header = bytearray(254)
    part_name = "%s.%s" % (file_name, chunk_index)
    header_encoded = base64.b64encode(part_name.encode("utf-8"))
    for index, byte in enumerate(header_encoded):
        header[index] = byte

    payload = header
    payload.extend(chunk)
    payload = gzip.compress(payload)
    payload = bytearray(payload)
    while len(payload) < 1024:
        payload.append(0)
    payload = bytes(payload)

    qr = segno.make(payload, error="H")
    qr.save(target_path % chunk_index, scale=10)
