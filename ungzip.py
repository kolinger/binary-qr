import gzip
import sys

source_path = sys.argv[1] if len(sys.argv) > 1 else None

if source_path is None:
    print("ERROR: not file provided")
    exit(1)

with open(source_path, "rb") as source:
    payload = gzip.decompress(source.read())

target_path = source_path + ".raw"
with open(target_path, "wb") as target:
    target.write(payload)
